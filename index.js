const express = require('express');
const cors = require('cors');
const vm = require('vm');
const md5 = require('md5');
const assert = require('assert');
const PORT = process.env.PORT || 5000;

const questionPool = {
  1: {
    tests: {
      mock: [
        {
          params: [ 1, 4 ],
          expectedOutput: 5
        },
        {
          params: [ 0, 2 ],
          expectedOutput: 2
        }
      ],
      real: [
        {
          params: [ 1, 2 ],
          expectedOutput: 3
        },
        {
          params: [ 3, 2 ],
          expectedOutput: 5
        }
      ],
    },
    timeout: 1000,
    visible: {
      description: 'Create a program that returns the sum of two inputted numbers.',
      samples: {
        input: [ '1 4', '0 2' ],
        output: [ '5', '2' ]
      },
      snippet: 'function main(a, b) {\n\treturn 0;\n}',
    }
  },
  2: {
    tests: {
      mock: [
        {
          params: [ 0 ],
          expectedOutput: 0
        },
        {
          params: [ 6 ],
          expectedOutput: 8
        },
        {
          params: [ 7 ],
          expectedOutput: 13
        }
      ],
      real: [
        {
          params: [ 13 ],
          expectedOutput: 233
        },
        {
          params: [ 15 ],
          expectedOutput: 610
        },
        {
          params: [ 17 ],
          expectedOutput: 1597
        }
      ],
    },
    timeout: 5000,
    visible: {
      description: 'Create a program that returns the nth number of a fibonacci sequence.\n\nThe sequence is as follow:\n0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...',
      samples: {
        input: [ '0', '6', '7' ],
        output: [ '0', '8', '13' ]
      },
      snippet: 'function main(n) {\n\treturn 0;\n}',
    }
  },
  3: {
    tests: {
      mock: [
        {
          params: [ [1, 2, 3], [3, 4, 5] ],
          expectedOutput: [3]
        },
        {
          params: [ [1, 1, 2], [1, 2, 3] ],
          expectedOutput: [1, 2]
        },
        {
          params: [ [1, 2, 3], [4, 5, 6] ],
          expectedOutput: []
        }
      ],
      real: [
        {
          params: [ [1, 1, 2, 2, 3], [2] ],
          expectedOutput: [2]
        },
        {
          params: [ [], [1, 2, 3] ],
          expectedOutput: []
        },
      ],
    },
    timeout: 5000,
    visible: {
      description: 'Create a program that returns the duplicate elements found in 2 arrays of number.',
      samples: {
        input: [ '[1, 2, 3] [3, 4, 5]', '[1, 1, 2] [1, 2, 3]', '[1, 2, 3] [4, 5, 6]' ],
        output: [ '[3]', '[1, 2]', '[]' ]
      },
      snippet: 'function main(a, b) {\n\treturn [];\n}',
    }
  }
};

function checkAnswer(questionID, answer, isMock) {
  const questionData = getQuestionData(questionID);

  const hash = md5(answer);
  const inputVarName = `input_${hash}`;
  const outputVarName = `output_${hash}`;

  const { tests, timeout } = questionData;

  // Concatenate script to run main() and store the output
  answer += `var ${outputVarName} = main(...${inputVarName});`;

  let script, scriptError;
  try {
    script = new vm.Script(answer);
  } catch (error) {
    scriptError = error.message;
  }

  const result = [];
  const testsData = isMock? tests.mock : tests.real;
  let successCount = 0;
  const totalCount = testsData.length;
  testsData.forEach((test) => {
    const { params, expectedOutput } = test;
    const passedArguments = {
      [`${inputVarName}`]: JSON.parse(JSON.stringify(params))
    };
    const context = new vm.createContext(passedArguments);

    const resultObj = isMock ? {
      params, 
      expectedOutput,
      success: false
    } : {
      expectedOutput,
      success: false
    };

    if (scriptError) {
      result.push({
        ...resultObj,
        error: scriptError
      });
    } else {
      try {
        script.runInContext(context, { timeout });
        const compiledOutput = passedArguments[outputVarName];
        let success = true;
        try {
          assert.deepEqual(compiledOutput, expectedOutput);
          successCount += 1;
        } catch(_) {
          success = false;
        }
        result.push({
          ...resultObj,
          compiledOutput,
          success
        });
      } catch (error) {
        result.push({
          ...resultObj,
          error: error.message
        });
      }
    }
  });

  return { 
    result, 
    successCount, 
    totalCount,
    clear: successCount === totalCount,
    isMock: Boolean(isMock)
  };
}

function getQuestionData(questionID) {
  const questionData = questionPool[questionID];

  //  If there is no questionData with the given ID, throw an error
  if (!questionData) throw new Error('Invalid questionID');

  return questionData;
}

express()
  .use(express.urlencoded({ extended: false }))
  .use(express.json())
  .use(cors())
  .post('/reflect', (req, res) => {
    res.end(JSON.stringify(req.body));
  })
  .get('/question/:id', (req, res) => {
    const { id } = req.params;
    try {
      const result = getQuestionData(id);
      res.end(JSON.stringify({ result: result.visible }));
    } catch (error) {
      res.end(JSON.stringify({ error: error.message }));
    }
  })
  .post('/submit/:id', (req, res) => {
    const { id } = req.params;
    const { answer, isMock } = req.body;
    try {
      const result = checkAnswer(id, answer, isMock);
      res.end(JSON.stringify(result));
    } catch (error) {
      res.end(JSON.stringify({ error: error.message }));
    }
  })
  .get('/', (_, res) => res.json({ foo: "bar" }))
  .listen(PORT, () => console.log(`Listening on ${ PORT }`));
