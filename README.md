# NodeJS JS Compiler

A NodeJS server used for verifying answers written in `JavaScript` for https://gitlab.com/Miscavel/online-editor.

A sample server is live at https://nodejs-js-compiler.herokuapp.com/

# Routes

`/`: Healthcheck endpoint

`/reflect`: Used for testing `CORS` on `POST` requests. Re-sends the body content back to the client.
```
Example:
link: https://nodejs-js-compiler.herokuapp.com/reflect
body: { "foo": "bar" }
response: { "foo": "bar" }
```

`/question/:id`: Used to get the description and sample input - output of the question with the given id.
```
Example:
link: https://nodejs-js-compiler.herokuapp.com/question/1
response: {
    "result": {
        "description": "Create a program that returns the sum of two inputted numbers.",
        "samples": {
            "input": [
                "1 4",
                "0 2"
            ],
            "output": [
                "5",
                "2"
            ]
        },
        "snippet": "function main(a, b) {\n\treturn 0;\n}"
    }
}
```

`/submit/:id`: Used to submit answer for the question with the given id.
```
Example (real tests):
link: https://nodejs-js-compiler.herokuapp.com/submit/1
body: { "answer": "function main(a, b) { return a + b; }" }
response: {
    "result": [
        {
            "compiledOutput": 3,
            "expectedOutput": 3,
            "success": true
        },
        {
            "compiledOutput": 5,
            "expectedOutput": 5,
            "success": true
        }
    ],
    "successCount": 2,
    "totalCount": 2,
    "clear": true,
    "isMock": false
}

Example (mock tests with exposed params):
link: https://nodejs-js-compiler.herokuapp.com/submit/1
body: { "answer": "function main(a, b) { return a + b; }", isMock: true }
response: {
    "result": [
        {
            "params": [
                1,
                4
            ],
            "expectedOutput": 5,
            "success": true,
            "compiledOutput": 5
        },
        {
            "params": [
                0,
                2
            ],
            "expectedOutput": 2,
            "success": true,
            "compiledOutput": 2
        }
    ],
    "successCount": 2,
    "totalCount": 2,
    "clear": true,
    "isMock": true
}
```
